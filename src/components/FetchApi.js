import React, { Component } from "react";

class FetchApi extends Component {

    fetchApi = async (url, body) => {
        const response = await fetch(url, body);

        const data = await response.json();

        return data;
    }

    getDetailBtnHandler = () => {
        // fetch('https://jsonplaceholder.typicode.com/posts/1')
        //     .then((response) => response.json())
        //     .then((json) => console.log(json))
        //     .then(() => {
        //         fetch('https://jsonplaceholder.typicode.com/posts/2')
        //             .then((response) => response.json())
        //             .then((json) => console.log(json))
        //     })
        //     .catch((error) => {
        //         console.error(error);
        //     });

        this.fetchApi('https://jsonplaceholder.typicode.com/posts/1')
            .then((data) => {
                console.log(data);
            })
            .catch((error) => {
                console.error(error);
            });
    }

    getAllBtnHandler = () => {
        this.fetchApi('https://jsonplaceholder.typicode.com/posts')
            .then((data) => {
                console.log(data);
            })
            .catch((error) => {
                console.error(error);
            });
    }

    createBtnHandler = () => {
        const body = {
            method: 'POST',
            body: JSON.stringify({
                title: 'foo',
                body: 'bar',
                userId: 1,
            }),
            headers: {
                'Content-type': 'application/json; charset=UTF-8',
            },
        }

        this.fetchApi('https://jsonplaceholder.typicode.com/posts', body)
            .then((data) => {
                console.log(data);
            })
            .catch((error) => {
                console.error(error);
            });
    }

    updateBtnHandler = () => {
        const body = {
            method: 'PUT',
            body: JSON.stringify({
                title: 'foo',
                body: 'bar',
                userId: 1,
            }),
            headers: {
                'Content-type': 'application/json; charset=UTF-8',
            },
          }

        this.fetchApi('https://jsonplaceholder.typicode.com/posts/1', body)
            .then((data) => {
                console.log(data);
            })
            .catch((error) => {
                console.error(error);
            });
    }

    deleteBtnHandler = () => {
        const body = {
            method: 'DELETE'
          }

        this.fetchApi('https://jsonplaceholder.typicode.com/posts/1', body)
            .then((data) => {
                console.log(data);
            })
            .catch((error) => {
                console.error(error);
            });
    }

    render() {
        return (
            <React.Fragment>
                <div className="row">
                    <h3>Fetch API</h3>
                </div>
                <div className="row">
                    <div className="col">
                        <button className="btn btn-info" onClick={this.getDetailBtnHandler}>Get Detail</button>
                    </div>
                    <div className="col">
                        <button className="btn btn-info" onClick={this.getAllBtnHandler}>Get All</button>
                    </div>
                    <div className="col">
                        <button className="btn btn-info" onClick={this.createBtnHandler}>Create</button>
                    </div>
                    <div className="col">
                        <button className="btn btn-info" onClick={this.updateBtnHandler}>Update</button>
                    </div>
                    <div className="col">
                        <button className="btn btn-info" onClick={this.deleteBtnHandler}>Delete</button>
                    </div>
                </div>
            </React.Fragment>
        )
    }
}

export default FetchApi;